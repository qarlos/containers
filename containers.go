package containers

// IntStack is a stack of ints (LIFO)
type IntStack []int

func (s *IntStack) Push(v int) {
	(*s) = append(*s, v)
}

func (s *IntStack) Pop() (int, bool) {
	if len(*s) == 0 {
		return 0, false
	}
	v := (*s)[len(*s)-1]
	(*s) = (*s)[:len(*s)-1]
	return v, true
}
